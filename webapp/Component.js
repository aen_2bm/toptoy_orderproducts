jQuery.sap.declare("retail.store.orderproduct.RTST_ORDER_PRODExtension.Component");

(function() {
jQuery.sap.registerModulePath("sap.retail.store.lib.reuse", "/sap/bc/ui5_ui5/sap/rtst_reuse/sap/retail/store/lib/reuse/");
}());

//	.sap.registerModulePath("sap.retail.store.lib.reuse", p + "../rtst_reuse/sap/retail/store/lib/reuse/");


// use the load function for getting the optimized preload file if present
sap.ui.component.load({
	name: "retail.store.orderproduct",
	// Use the below URL to run the extended application when SAP-delivered application is deployed on SAPUI5 ABAP Repository
	url: "/sap/bc/ui5_ui5/sap/RTST_ORDER_PROD"
		// we use a URL relative to our own component
		// extension application is deployed with customer namespace
});

this.retail.store.orderproduct.Component.extend("retail.store.orderproduct.RTST_ORDER_PRODExtension.Component", {
	metadata: {
		version: "1.0",
		config: {},

		customizing: {}
	},
});